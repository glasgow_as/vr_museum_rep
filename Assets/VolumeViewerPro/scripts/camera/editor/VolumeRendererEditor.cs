﻿///-----------------------------------------------------------------
/// Namespace:          VolumeViewer
/// Class:              VolumeRendererEditor
/// Description:        Custom editor for the VolumeRenderer class.
/// Author:             LISCINTEC
///                         http://www.liscintec.com
///                         info@liscintec.com
/// Date:               Nov 2017
/// Notes:              -
/// Version:   			1.0.2
/// 
/// This file is part of the Volume Viewer Pro package.
/// Volume Viewer Pro is a Unity Asset Store product:
/// https://www.assetstore.unity3d.com/#!/content/83185
/// 
/// By installing, copying, accessing, downloading or otherwise 
/// using this Assets, you agrees to be bound by the provisions
/// of the Asset STORE End User License Agreement:
/// https://unity3d.com/legal/as_terms
/// 
/// You may only use this Assets as incorporated and embedded
/// component of electronic games and interactive media.
/// 
/// You may not reproduce, distribute, sublicense, rent, lease,
/// lend or otherwise share this Asset or parts of it.
/// It is emphasized that you shall not be entitled to distribute
/// or transfer in any way (including, without, limitation by way 
/// of sublicense) the Assets in any other way than as integrated 
/// components of electronic games and interactive media.
///-----------------------------------------------------------------

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace VolumeViewer
{
    [CustomEditor(typeof(VolumeRenderer), true)]
    [CanEditMultipleObjects]
    public class VolumeRendererEditor : Editor
    {

        bool showEvents;

        SerializedProperty volumeObjectsProp;
        SerializedProperty enableCullingProp;
        SerializedProperty enableDepthTestProp;
        SerializedProperty leapProp;

        SerializedProperty volumeObjectsChangedProp;
        SerializedProperty enableCullingChangedProp;
        SerializedProperty enableDepthTestChangedProp;
        SerializedProperty leapChangedProp;

        void OnEnable()
        {
            volumeObjectsProp = serializedObject.FindProperty("_volumeObjects");
            enableCullingProp = serializedObject.FindProperty("_enableCulling");
            enableDepthTestProp = serializedObject.FindProperty("_enableDepthTest");
            leapProp = serializedObject.FindProperty("_leap");
            volumeObjectsChangedProp = serializedObject.FindProperty("_volumeObjectsChanged");
            enableCullingChangedProp = serializedObject.FindProperty("_enableCullingChanged");
            enableDepthTestChangedProp = serializedObject.FindProperty("_enableDepthTestChanged");
            leapChangedProp = serializedObject.FindProperty("_leapChanged");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.Space();
            EditorGUILayout.PropertyField(enableCullingProp);
            EditorGUILayout.PropertyField(enableDepthTestProp);
            EditorGUILayout.PropertyField(leapProp);
            EditorGUILayout.PropertyField(volumeObjectsProp, true);

            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Events: ", EditorStyles.boldLabel);
            showEvents = EditorGUILayout.Foldout(showEvents, "Events");
            if (showEvents)
            {
                EditorGUILayout.PropertyField(enableCullingChangedProp);
                EditorGUILayout.PropertyField(enableDepthTestChangedProp);
                EditorGUILayout.PropertyField(leapChangedProp);
                EditorGUILayout.PropertyField(volumeObjectsChangedProp);
            }
            serializedObject.ApplyModifiedProperties();
        }
    }
}