﻿////--------------------------------------------------------------------
/// Namespace:          
/// Class:              MouseOverHandle
/// Description:        Hides a Handle unless the cursor is inside of a
///                         certain RectTransform. If a mouse button is
///                         pushed down while inside the RectTransform,
///                         the Handle is kept visible until that button 
///                         is released, regardless of the cursor's 
///                         position.
/// Author:             LISCINTEC
///                         http://www.liscintec.com
///                         info@liscintec.com
/// Date:               Nov 2017
/// Notes:              -
/// Version:   			1.0.2
/// 
/// This file is part of the Volume Viewer Pro package.
/// Volume Viewer Pro is a Unity Asset Store product:
/// https://www.assetstore.unity3d.com/#!/content/83185
/// 
/// By installing, copying, accessing, downloading or otherwise 
/// using this Assets, you agrees to be bound by the provisions
/// of the Asset STORE End User License Agreement:
/// https://unity3d.com/legal/as_terms
/// 
/// You may only use this Assets as incorporated and embedded
/// component of electronic games and interactive media.
/// 
/// You may not reproduce, distribute, sublicense, rent, lease,
/// lend or otherwise share this Asset or parts of it.
/// It is emphasized that you shall not be entitled to distribute
/// or transfer in any way (including, without, limitation by way 
/// of sublicense) the Assets in any other way than as integrated 
/// components of electronic games and interactive media.
///-----------------------------------------------------------------

using UnityEngine;
using UnityEngine.EventSystems;

public class MouseOverHandle : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler, IPointerUpHandler {

    [SerializeField]
    public RectTransform m_HandleRect;

    private RectTransform imageTransform;
    private DrivenRectTransformTracker m_Tracker;
    private Vector3[] imageCorners;
    private bool insideImage = false;
    private bool buttonDown = false;

	// Initialization
	void Start () {
	    imageTransform = transform as RectTransform;
        m_Tracker.Add(this, m_HandleRect, DrivenTransformProperties.Anchors);
        imageCorners = new Vector3[4];
	}

    public void OnPointerDown(PointerEventData eventData)
    {
        buttonDown = true;
        m_HandleRect.gameObject.SetActive(true);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        buttonDown = false;
        if(insideImage == false)
        {
            m_HandleRect.gameObject.SetActive(false);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        insideImage = true;
        m_HandleRect.gameObject.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        insideImage = false;
        if(buttonDown == false)
        {
            m_HandleRect.gameObject.SetActive(false);
        }
    }

    Vector2 ClampToImage (Vector2 position)
    {
        imageTransform.GetWorldCorners(imageCorners);

        float clampedX = Mathf.Clamp (position.x, imageCorners[0].x, imageCorners[2].x);
        float clampedY = Mathf.Clamp (position.y, imageCorners[0].y, imageCorners[2].y);

        Vector2 newPointerPosition = new Vector2 (clampedX, clampedY);
        return newPointerPosition;
    }

    void Update()
    {

        if (imageTransform == null || (insideImage == false && buttonDown == false))
        {
            return;
        }
        
        m_HandleRect.position = ClampToImage(Input.mousePosition);
        
    }
}
