﻿////--------------------------------------------------------------------
/// Namespace:          
/// Class:              RotationPlane
/// Description:        Changes the rotation of a VolumeComponent.
/// Author:             LISCINTEC
///                         http://www.liscintec.com
///                         info@liscintec.com
/// Date:               Nov 2017
/// Notes:              -
/// Version:   			1.0.2
/// 
/// This file is part of the Volume Viewer Pro package.
/// Volume Viewer Pro is a Unity Asset Store product:
/// https://www.assetstore.unity3d.com/#!/content/83185
/// 
/// By installing, copying, accessing, downloading or otherwise 
/// using this Assets, you agrees to be bound by the provisions
/// of the Asset STORE End User License Agreement:
/// https://unity3d.com/legal/as_terms
/// 
/// You may only use this Assets as incorporated and embedded
/// component of electronic games and interactive media.
/// 
/// You may not reproduce, distribute, sublicense, rent, lease,
/// lend or otherwise share this Asset or parts of it.
/// It is emphasized that you shall not be entitled to distribute
/// or transfer in any way (including, without, limitation by way 
/// of sublicense) the Assets in any other way than as integrated 
/// components of electronic games and interactive media.
///-----------------------------------------------------------------

using UnityEngine;

public class RotationPlane : MonoBehaviour {

    public Transform[] objTransform;
    public float rotationSpeed;

    public void changeRotation(Vector2 delta)
    {
        if(objTransform.Length < 1)
        {
            return;
        }
        objTransform[0].Rotate(objTransform[0].InverseTransformDirection(Vector3.down), delta.x * rotationSpeed);
        objTransform[0].Rotate(objTransform[0].InverseTransformDirection(Vector3.right), delta.y * rotationSpeed);
        for (int i = 1; i < objTransform.Length; i++)
        {
            objTransform[i].localRotation = objTransform[0].localRotation;
        }
    }

}
