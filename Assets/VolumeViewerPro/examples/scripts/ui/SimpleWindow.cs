﻿////--------------------------------------------------------------------
/// Namespace:          
/// Class:              SimpleWindow
/// Description:        Simple window mechanic to drag something around
///                         on a title bar.
/// Author:             LISCINTEC
///                         http://www.liscintec.com
///                         info@liscintec.com
/// Date:               Nov 2017
/// Notes:              -
/// Version:   			1.0.2
/// 
/// This file is part of the Volume Viewer Pro package.
/// Volume Viewer Pro is a Unity Asset Store product:
/// https://www.assetstore.unity3d.com/#!/content/83185
/// 
/// By installing, copying, accessing, downloading or otherwise 
/// using this Assets, you agrees to be bound by the provisions
/// of the Asset STORE End User License Agreement:
/// https://unity3d.com/legal/as_terms
/// 
/// You may only use this Assets as incorporated and embedded
/// component of electronic games and interactive media.
/// 
/// You may not reproduce, distribute, sublicense, rent, lease,
/// lend or otherwise share this Asset or parts of it.
/// It is emphasized that you shall not be entitled to distribute
/// or transfer in any way (including, without, limitation by way 
/// of sublicense) the Assets in any other way than as integrated 
/// components of electronic games and interactive media.
///-----------------------------------------------------------------

using UnityEngine;
using UnityEngine.EventSystems;

public class SimpleWindow : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler {
    
    [SerializeField] private RectTransform dragRectTransform;
    [SerializeField] private GameObject windowContent;

    private RectTransform windowRectTransform;
    private RectTransform parentRectTransform;
    private Vector2 pointerOffset;
    private bool DownOnDrag;

    void Awake()
    {
        DownOnDrag = false;
        windowRectTransform = transform as RectTransform;
        parentRectTransform = windowRectTransform.parent as RectTransform;
    }

    public void OnPointerDown (PointerEventData data) {
        transform.SetAsLastSibling ();
        if (RectTransformUtility.RectangleContainsScreenPoint(dragRectTransform, data.position, data.enterEventCamera))
        {
            DownOnDrag = true;
            RectTransformUtility.ScreenPointToLocalPointInRectangle (windowRectTransform, data.position, data.pressEventCamera, out pointerOffset);
        }
    }

    public void OnPointerUp(PointerEventData data) {
        DownOnDrag = false;
    }

    public void OnDrag (PointerEventData data) {
        if(DownOnDrag)
        {
            Vector2 localPointerPosition;
            if (RectTransformUtility.ScreenPointToLocalPointInRectangle (parentRectTransform, ClampToParent(data.position), data.pressEventCamera, out localPointerPosition)) {
                windowRectTransform.localPosition = (localPointerPosition - pointerOffset);
            }
        }
    }

    public void DisplayContent(bool visible)
    {
        windowContent.SetActive(visible);
    }

    Vector2 ClampToParent (Vector2 pos) {

        Vector3[] parentCorners = new Vector3[4];
        parentRectTransform.GetWorldCorners (parentCorners);
        
        Vector2 newPos = Vector2.zero;
        newPos.x = Mathf.Clamp (pos.x, parentCorners[0].x, parentCorners[2].x);
        newPos.y = Mathf.Clamp (pos.y, parentCorners[0].y, parentCorners[2].y);

        return newPos;
    }
}
