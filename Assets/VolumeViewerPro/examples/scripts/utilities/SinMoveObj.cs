﻿////--------------------------------------------------------------------
/// Namespace:          
/// Class:              SinMoveObj
/// Description:        Moves its GameObject in a sinusoidal fashion.
/// Author:             LISCINTEC
///                         http://www.liscintec.com
///                         info@liscintec.com
/// Date:               Nov 2017
/// Notes:              -
/// Version:   			1.0.2
/// 
/// This file is part of the Volume Viewer Pro package.
/// Volume Viewer Pro is a Unity Asset Store product:
/// https://www.assetstore.unity3d.com/#!/content/83185
/// 
/// By installing, copying, accessing, downloading or otherwise 
/// using this Assets, you agrees to be bound by the provisions
/// of the Asset STORE End User License Agreement:
/// https://unity3d.com/legal/as_terms
/// 
/// You may only use this Assets as incorporated and embedded
/// component of electronic games and interactive media.
/// 
/// You may not reproduce, distribute, sublicense, rent, lease,
/// lend or otherwise share this Asset or parts of it.
/// It is emphasized that you shall not be entitled to distribute
/// or transfer in any way (including, without, limitation by way 
/// of sublicense) the Assets in any other way than as integrated 
/// components of electronic games and interactive media.
///-----------------------------------------------------------------

using UnityEngine;

public class SinMoveObj : MonoBehaviour {

    public Vector3 moveDirection;
    public float multiplier;

    Vector3 originalPosition;
    float elapsedTime;

    // Initialization
    void Start()
    {
        originalPosition = transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        elapsedTime += Time.deltaTime;
        transform.localPosition = Mathf.Sin(elapsedTime * multiplier) * moveDirection + originalPosition;
        if (elapsedTime > 2 * Mathf.PI)
        {
            elapsedTime -= 2 * Mathf.PI;
        }
    }
}
