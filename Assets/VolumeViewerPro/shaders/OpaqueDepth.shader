﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

///-----------------------------------------------------------------
/// Shader:             OpaqueDepth
/// Description:        Renders the depth of opaque objects.
/// Author:             LISCINTEC
///                         http://www.liscintec.com
///                         info@liscintec.com
/// Date:               Nov 2017
/// Notes:              -
/// Version:   			1.0.2
/// 
/// This file is part of the Volume Viewer Pro package.
/// Volume Viewer Pro is a Unity Asset Store product:
/// https://www.assetstore.unity3d.com/#!/content/83185
/// 
/// By installing, copying, accessing, downloading or otherwise 
/// using this Assets, you agrees to be bound by the provisions
/// of the Asset STORE End User License Agreement:
/// https://unity3d.com/legal/as_terms
/// 
/// You may only use this Assets as incorporated and embedded
/// component of electronic games and interactive media.
/// 
/// You may not reproduce, distribute, sublicense, rent, lease,
/// lend or otherwise share this Asset or parts of it.
/// It is emphasized that you shall not be entitled to distribute
/// or transfer in any way (including, without, limitation by way 
/// of sublicense) the Assets in any other way than as integrated 
/// components of electronic games and interactive media.
///-----------------------------------------------------------------

Shader "Hidden/VolumeViewer/OpaqueDepth" 
{
	SubShader 
	{
		Tags { "RenderType"="Opaque" }

		Pass 
		{
			Cull Back
			ZTest Less
			
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				
				struct u2v {
					float4 pos : POSITION;
				};

				struct v2f {
					float4 pos : SV_POSITION;
					float  depth : TEXCOORD0;
				};

				v2f vert(u2v v) 
				{
					v2f o;
					o.pos = UnityObjectToClipPos(v.pos);
					o.depth = -(mul(UNITY_MATRIX_MV, v.pos).z * _ProjectionParams.w);
					return o;
				}
				
				half4 frag(v2f i) : COLOR
				{  
					return i.depth;
				}

			ENDCG
		}
	} 
	FallBack Off
}